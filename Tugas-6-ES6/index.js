// soal 1
let LuasPersegi = (x) => {
return x*x;
}
let KelilingPersegi = (x) =>{
return x+x+x+x;
}

//jawaban no 1
console.log(LuasPersegi(4));
console.log(KelilingPersegi(5));

//soal 2
// menyederhanakan code
//jawaban
const newFunction = (fisrtName, lastname) =>{
return {
    fisrtName ,
    lastname,
    fullName(){
        console.log(fisrtName + " " + lastname)
        return
    }
} 
}

newFunction("William", "Imoh").fullName()

//soal 3
//gunakan metode destruction dalam ES6 untuk menyederhanakan kode berikut
/*
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const address = newObject.address;
const hobby = newObject.hobby;
*/

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
//jawaban no 3
const {firstName, lastName, address , hobby} = newObject

console.log(firstName, lastName, address, hobby)

//soal no 4
// combinasican dua array berikut dengan array spreading ES6
/*
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)
*/

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// jawaban n0 4
let combined = [...west, ...east];
console.log(combined);

//soal no 5
//sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6
/*
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
*/

//jawaban no 5
const planet = "earth" 
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adiposcing elit, ${planet}`
console.log(before)