//soal nomor 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//gabungkan variabel tersebut sehingga mengahsilkan output saya senang belajar JAVASCRIPT
//jawaban soal 1
var kata1 = pertama.substr(0, 4);
var kata2 = pertama.substr(12, 6);
var kata3 = kedua.substr(0, 7);
var kata4 = kedua.substr(8, 10).toUpperCase();

var kalimat = kata1+" "+kata2+" "+kata3+" "+kata4;
console.log(kalimat);

//soal nomor 2
var kataPertama = 10;
var kataKedua = 2;
var kataKetiga = 4;
var kataKeempat = 6;

//jawaban soal 2
var hasil = (kataPertama+kataKeempat)+(kataKedua*kataKetiga) ;
var strHasil = String(hasil);

//hasilnya harus 24 tidak boleh lebih dari 3 operasi
console.log(strHasil);

//soal nomor 3
var kalimat = "wah javascript itu keren sekali";
var kataPertama = kalimat.substring(0,3);
//jawaban no 3
var kataKedua = kalimat.substr(4,10);
var kataKetiga = kalimat.substr(15,3);
var kataKeempat = kalimat.substr(19,5);
var kataKelima = kalimat.substr(25,6);

console.log("Kata Pertama : "+kataPertama)
console.log("Kata Kedua : "+kataKedua)
console.log("Kata Ketiga : "+kataKetiga)
console.log("Kata Keempat : "+kataKeempat)
console.log("Kata Kelima : "+kataKelima)

/*
otputnya harus seperti ini
Kata Pertama: wah
Kata Kedua: javascript
Kata Ketiga: itu
Kata Keempat: keren
Kata Kelima: sekali
*/ 

