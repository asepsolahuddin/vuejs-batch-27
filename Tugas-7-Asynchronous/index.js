//soal no 1
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
const batas_waktu = 10000

//jawaban no 1
readBooks(batas_waktu, books[0] ,(sisawaktu1) => {
    readBooks(sisawaktu1, books[1] ,(sisawaktu2) => {
        readBooks(sisawaktu2, books[2] ,(sisawaktu3) => {
            readBooks(sisawaktu3, books[3] ,(waktu_habis)=> {
                console.log(`waktu anda ${waktu_habis} anda sudah tidak punya waktu membaca`)
            })
        })
    })
})